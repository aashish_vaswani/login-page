<?php
require "app/init.php";

if($auth->check()) {
    $user = $auth->user();

    $auth->signout();
    //delete the token from database
    $tokenHandler->deleteToken($user->id, 1);
    //clear the token cookie from the browser history
    unset($_COOKIE['token']);   //this will only delete the cookie created in the $_COOKIE[] Array.
    setcookie('token', '' , time()-3600);   //to actually delete the cookie. we have to do this. pass the same name and exp time as something in negative

    header("Location: signin.php");
}
else
{
    echo "Unauthorized Access!";
}
