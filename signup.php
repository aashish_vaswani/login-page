<?php
require "app/init.php";
if(!empty($_POST))
{
    $validator->check($_POST, [
        'email' => [
            'required' => true,
            'maxlength' => 200,
            'email' => true,
            'unique' => 'users'
        ],
        'username' => [
            'required' => true,
            'maxlength' => 20,
            'minlength' => 3,
            'unique' => 'users'
        ],
        'password' => [
            'required' => true,
            'minlength' => 8,
            'maxlength' => 255
        ]
    ]);

    if($validator->fails()) {
        print_r($validator->errors()->all());
    } else {
        $email = $_POST['email'];
        $username = $_POST['username'];
        $password = $_POST['password'];

        $created = $auth->create([
            'email' => $email,
            'username' => $username,
            'password' => $password
        ]);

        if($created) {
            #die("hello!");
            header("Location: securedpage.php");
        } else {
            echo "There was some issue while creating your user!";
        }
    }
}

?>
<html>
    <head>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="css/sigin.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
    </head>
    <body>

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="users.svg" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form action="signup.php" method="POST">
    <input type="text" id="login" class="fadeIn second" name="email" placeholder="Email">  
    <?php
                    if($validator->fails() && $validator->errors()->has('email')) {
                        echo $validator->errors()->first('email');
                    }
                ?>
    <input type="text" id="login" class="fadeIn second" name="username" placeholder="Username">
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
      <!-- <div class="row align-items-center remember"> -->
            
		<!-- </div> -->
      <input type="submit" class="fadeIn fourth" style = "margin-top:3em;" value="Log In"><br>
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="signin.php">Already have an account?</a>
    </div>

  </div>
</div>

    </body>
</html>