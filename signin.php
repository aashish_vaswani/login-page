<?php
require "app/init.php";

if(!empty($_POST))
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rememberMe = $_POST['rem'];
    $status = $auth->signin($username, $password);
    if($status)
    {
        if($rememberMe)
        {
            $user = $userHelper->findUserByUsername($username);
            $token = $tokenHandler->createRememberMeToken($user->id);
            setcookie("token" , $token['token'] , time()+1800);
        }
        header('Location: index.php');
    }
    else
    {
        echo "Wrong Username/ Password!";
    }
}
?>

<html>
    <head>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="css/sigin.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
    </head>
    <body>
    <?php if(isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'], 1)): ?>

<h3>You are already signed in!</h3>

<?php else: ?>
<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="users.svg" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form action="signin.php" method="POST">
      <input type="text" id="login" class="fadeIn second" name="username" placeholder="Username">
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
      <!-- <div class="row align-items-center remember"> -->
            
		<!-- </div> -->
      <input type="submit" class="fadeIn fourth" style = "margin-top:3em;" value="Log In"><br>
      <input type="checkbox" class="fadeIn fifth" name = "rem" checked value="Remember Me"> <span class="fadeIn fifth">Remember Me</span>
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="forgot_password.php" style="float:left;"le>Forgot Password?</a>
      <a class="underlineHover" href="signup.php" style="float:right;">Not a member?</a>
    </div>

  </div>
</div>
<?php endif; ?>
    </body>
</html>