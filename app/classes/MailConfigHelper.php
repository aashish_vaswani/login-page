<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class MailConfigHelper
{
    public static function getMailer(int $debugMode=0): PHPMailer
    {
        $mail = new PHPMailer();

        $mail->isSMTP();

        $mail->Host = 'smtp.mailtrap.io';
        $mail->Port = 2525;
        $mail->SMTPAuth = true;
        $mail->Username = 'ea5516e55031a4';
        $mail->Password = 'cc8be6ed3e84c4';
        $mail->SMTPSecure = 'tls';
        $mail->isHtml(true);
        $mail->setFrom('admin@studylinkclasses.com' , 'Admin<SL Team>');

        return $mail;
    }
}